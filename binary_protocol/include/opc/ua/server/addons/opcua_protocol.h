/// @author Alexander Rykovanov 2013
/// @email rykovanov.as@gmail.com
/// @brief Endpoints addon.
/// @license GNU LGPL
///
/// Distributed under the GNU LGPL License
/// (See accompanying file LICENSE or copy at
/// http://www.gnu.org/licenses/lgpl.html)
///

#pragma once

namespace OpcUa
{
  namespace Server
  {

    const char OpcUaProtocolAddonID[] = "opcua_protocol";

  } // namespace Server
} // nmespace OpcUa
